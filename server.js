const express = require('express');

const app = express();
const PORT = process.env.NODE_ENV === 'production'
    ? (process.env.PORT || 80)
    : (process.env.PORT || 3000);

app.get('/', (req, res) => {
    res.send('Hello World!');
});

app.use((req, res, next) => {
    res.status(404).send('Not Found');
});

app.listen(PORT, () => {
    console.log(`Server is listening on port ${PORT}...`);
});
