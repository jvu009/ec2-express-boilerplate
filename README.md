# ec2-express-boilerplate

## Getting Started

Clone the repository: `git clone git@bitbucket.org:jvu009/ec2-express-boilerplate.git && cd ec2-express-boilerplate`

Install dependencies: `npm install`

Start the app: `npm start`
